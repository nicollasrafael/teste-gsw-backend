﻿using NUnit.Framework;
using System.Collections.Generic;
using Teste.Domain.Entities;
using Teste.Domain.Entities.ValueObjects;

namespace Teste.UnitTest.Domain.Entities
{
    public class CustomerTest
    {
        [Test]
        public void CustomerShouldInitializeSuccessfully()
        {
            // Arrange & Act
            var personName = new PersonName("Foo Bar");
            var address = new Address("123 St.", "Tampa", "Florida", "00000");
            var customer = new Customer(personName, address);

            // Assert
            Assert.AreEqual(customer.Id, default(int));

            Assert.IsNotNull(customer.Name);
            Assert.IsNotNull(customer.Address);
            Assert.IsNotNull(customer.Emails);
            Assert.IsNotNull(customer.Phones);

            Assert.AreEqual(customer.Name.FullName, personName.FullName);
            Assert.AreEqual(customer.Name.FirstName, personName.FirstName);
            Assert.AreEqual(customer.Name.LastName, personName.LastName);

            Assert.AreEqual(customer.Address.StreetAddress, address.StreetAddress);
            Assert.AreEqual(customer.Address.City, address.City);
            Assert.AreEqual(customer.Address.State, address.State);
            Assert.AreEqual(customer.Address.ZipCode, address.ZipCode);

            Assert.AreEqual(customer.Emails.Count, 0);
            Assert.AreEqual(customer.Phones.Count, 0);
        }

        [Test]
        public void CustomerShouldUpdatePersonNameSuccessfully()
        {
            // Arrange
            var address = new Address("123 St.", "Tampa", "Florida", "00000");
            var customer = new Customer(new PersonName("Foo Bar"), address);

            // Act
            var newPersonName = new PersonName("Foo Something");
            customer.UpdatePersonName(newPersonName);

            // Assert
            Assert.AreEqual(customer.Name.FullName, newPersonName.FullName);
            Assert.AreEqual(customer.Name.FirstName, newPersonName.FirstName);
            Assert.AreEqual(customer.Name.LastName, newPersonName.LastName);
        }

        [Test]
        public void CustomerShouldUpdateAddressSuccessfully()
        {
            // Arrange
            var customer = new Customer(new PersonName("Foo Bar"), new Address("123 St.", "Tampa", "Florida", "00000"));

            // Act
            var newAddress = new Address("123 St.", "Tampa", "Florida", "00000");
            customer.UpdateAddress(newAddress);

            // Assert
            Assert.AreEqual(customer.Address.StreetAddress, newAddress.StreetAddress);
            Assert.AreEqual(customer.Address.City, newAddress.City);
            Assert.AreEqual(customer.Address.State, newAddress.State);
            Assert.AreEqual(customer.Address.ZipCode, newAddress.ZipCode);
        }

        [Test]
        public void CustomerShouldUpdateEmailsSuccessfully()
        {
            // Arrange
            var customer = new Customer(new PersonName("Foo Bar"), new Address("123 St.", "Tampa", "Florida", "00000"));
            var customerEmails = new List<Email>()
            {
                new Email("foo@bar.com"),
                new Email("foo@var.com")
            };

            // Act & Assert
            customer.UpdateEmails(customerEmails);
            Assert.AreEqual(customer.Emails.Count, 2);

            customerEmails.RemoveAt(0);
            customer.UpdateEmails(customerEmails);
            Assert.AreEqual(customer.Emails.Count, 1);
        }

        [Test]
        public void CustomerShouldUpdatePhonesSuccessfully()
        {
            // Arrange
            var customer = new Customer(new PersonName("Foo Bar"), new Address("123 St.", "Tampa", "Florida", "00000"));
            var customerPhones = new List<Phone>()
            {
                new Phone("00000000000000"),
                new Phone("11111111111111")
            };

            // Act & Assert
            customer.UpdatePhones(customerPhones);
            Assert.AreEqual(customer.Phones.Count, 2);

            customerPhones.RemoveAt(0);
            customer.UpdatePhones(customerPhones);
            Assert.AreEqual(customer.Phones.Count, 1);
        }
    }
}
