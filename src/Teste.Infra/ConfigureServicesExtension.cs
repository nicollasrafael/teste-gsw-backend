﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Teste.Domain.Repositories;
using Teste.Domain.Services;
using Teste.Infra.Data;
using Teste.Infra.Repositories;

namespace Teste.Infra
{
    public static class ConfigureServicesExtension
    {
        public static IServiceCollection ConfigureServicesIoc(this IServiceCollection services, IConfiguration configuration)
        {

            RegisterDatabase(services, configuration);
            RegisterServices(services);
            RegisterRepositories(services);

            return services;
        }

        private static void RegisterDatabase(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<TesteContext>(options => 
            {
                options
                    .UseSqlServer(configuration["ConnectionString"]);
            });
        }

        private static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<ICustomerService, CustomerService>();
        }

        private static void RegisterRepositories(IServiceCollection services)
        {
            services.AddScoped<ICustomerRepository, CustomerRepository>();
        }
    }
}
