﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Teste.Domain.Entities;

namespace Teste.Infra.Data.Configurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.OwnsOne(o => o.Name,
                sa =>
                {
                    sa.Property(p => p.FullName)
                        .HasColumnName("FullName")
                        .HasComputedColumnSql("CONCAT(FirstName, ' ', LastName) PERSISTED");

                    sa.Property(p => p.FirstName)
                        .HasColumnName("FirstName")
                        .HasMaxLength(50)
                        .IsRequired();

                    sa.Property(p => p.LastName)
                        .HasColumnName("LastName")
                        .HasMaxLength(50)
                        .IsRequired();
                });

            builder.OwnsOne(o => o.Address,
                sa =>
                {
                    sa.Property(p => p.StreetAddress)
                        .HasMaxLength(300)
                        .IsRequired();

                    sa.Property(p => p.City)
                        .HasMaxLength(50)
                        .IsRequired();

                    sa.Property(p => p.State)
                        .HasMaxLength(10)
                        .IsRequired();

                    sa.Property(p => p.ZipCode)
                        .HasMaxLength(35)
                        .IsRequired();
                });

            builder
                .HasMany(x => x.Emails)
                .WithOne(o => o.Customer)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasMany(x => x.Phones)
                .WithOne(o => o.Customer)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
