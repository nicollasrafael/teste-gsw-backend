﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Teste.Domain.Entities;

namespace Teste.Infra.Data.Configurations
{
    public class CustomerPhoneConfiguration : IEntityTypeConfiguration<CustomerPhone>
    {
        public void Configure(EntityTypeBuilder<CustomerPhone> builder)
        {
            builder
                .OwnsOne(x => x.Phone, 
                sa =>
                {
                    sa.Property(o => o.Number)
                        .HasColumnName("Number")
                        .HasMaxLength(15)
                        .IsRequired();
                });
        }
    }
}
