﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Teste.Domain.Entities;

namespace Teste.Infra.Data.Configurations
{
    public class CustomerEmailConfiguration : IEntityTypeConfiguration<CustomerEmail>
    {
        public void Configure(EntityTypeBuilder<CustomerEmail> builder)
        {
            builder
                .OwnsOne(x => x.Email, 
                sa =>
                {
                    sa.Property(o => o.Address)
                        .HasColumnName("Address")
                        .HasMaxLength(255)
                        .IsRequired();
                });
        }
    }
}
