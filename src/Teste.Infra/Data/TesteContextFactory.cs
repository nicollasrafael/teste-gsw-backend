﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Teste.Infra.Data
{
    public class TesteContextFactory : IDesignTimeDbContextFactory<TesteContext>
    {
        public TesteContext CreateDbContext(string[] args)
        {
            var configurationBuilder = new ConfigurationBuilder()
                .AddEnvironmentVariables("Teste_")
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<TesteContext>();
            optionsBuilder.UseSqlServer(configurationBuilder["ConnectionString"]);

            return new TesteContext(optionsBuilder.Options);
        }
    }
}
