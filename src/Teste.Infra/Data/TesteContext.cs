﻿using Microsoft.EntityFrameworkCore;
using Teste.Domain.Entities;
using Teste.Infra.Data.Configurations;

namespace Teste.Infra.Data
{
    public class TesteContext : DbContext
    {
        public TesteContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .ApplyConfiguration(new CustomerConfiguration())
                .ApplyConfiguration(new CustomerPhoneConfiguration())
                .ApplyConfiguration(new CustomerEmailConfiguration());
        }

        public DbSet<Customer> Customer { get; set; }
    }
}
