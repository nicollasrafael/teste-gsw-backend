﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Teste.Infra.Data;
using Teste.Domain.Entities;
using Teste.Domain.Repositories;
using System.Linq;

namespace Teste.Infra.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly TesteContext _context;

        public CustomerRepository(TesteContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await _context.Customer
                .AsNoTracking()
                .OrderByDescending(x => x.Id)
                .ToListAsync();
        }

        public async Task<Customer> GetByIdAsync(int customerId)
        {
            return await _context.Customer
                .Include(x => x.Emails)
                .Include(x => x.Phones)
                .FirstOrDefaultAsync(x => x.Id == customerId);
        }

        public async Task<Customer> CreateAsync(Customer customer)
        {
            var entity = _context.Customer.Add(customer);
            entity.State = EntityState.Added;

            await _context.SaveChangesAsync();

            return customer;
        }

        public async Task UpdateAsync(Customer customer)
        {
            var entity = _context.Customer.Update(customer);
            entity.State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Customer customer)
        {
            var entity = _context.Customer.Remove(customer);
            entity.State = EntityState.Deleted;

            await _context.SaveChangesAsync();
        }
    }
}
