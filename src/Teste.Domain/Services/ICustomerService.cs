﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Teste.Domain.Entities;
using Teste.Domain.Resources;

namespace Teste.Domain.Services
{
    public interface ICustomerService
    {
        Task<Customer> GetByIdAsync(int customerId);
        Task<IEnumerable<Customer>> GetAllAsync();
        Task<Customer> CreateAsync(CustomerRequestDto customerRequestDto);
        Task UpdateAsync(Customer customer, CustomerRequestDto customerRequestDto);
        Task DeleteAsync(Customer customer);
    }
}
