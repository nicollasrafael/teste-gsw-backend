﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Teste.Domain.Entities;
using Teste.Domain.Entities.ValueObjects;
using Teste.Domain.Repositories;
using Teste.Domain.Resources;

namespace Teste.Domain.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await _customerRepository.GetAllAsync();
        }

        public async Task<Customer> GetByIdAsync(int customerId)
        {
            return await _customerRepository.GetByIdAsync(customerId);
        }

        public async Task<Customer> CreateAsync(CustomerRequestDto customerRequestDto)
        {
            var customer = new Customer(
                new PersonName(customerRequestDto.Name), 
                new Address(
                    customerRequestDto.Address.StreetAddress, 
                    customerRequestDto.Address.City, 
                    customerRequestDto.Address.State, 
                    customerRequestDto.Address.ZipCode));

            customer.UpdateEmails(customerRequestDto.Emails.Select(emailAddress => new Email(emailAddress)));
            customer.UpdatePhones(customerRequestDto.Phones.Select(phoneNumber => new Phone(phoneNumber)));

            return await _customerRepository.CreateAsync(customer);
        }

        public async Task UpdateAsync(Customer customer, CustomerRequestDto customerRequestDto)
        {
            customer.UpdatePersonName(new PersonName(customerRequestDto.Name));

            customer.UpdateAddress(new Address(
                    customerRequestDto.Address.StreetAddress,
                    customerRequestDto.Address.City,
                    customerRequestDto.Address.State,
                    customerRequestDto.Address.ZipCode));

            customer.UpdateEmails(customerRequestDto.Emails.Select(emailAddress => new Email(emailAddress)));
            customer.UpdatePhones(customerRequestDto.Phones.Select(phoneNumber => new Phone(phoneNumber)));

            await _customerRepository.UpdateAsync(customer);
        }

        public async Task DeleteAsync(Customer customer)
        {
            await _customerRepository.DeleteAsync(customer);
        }
    }
}
