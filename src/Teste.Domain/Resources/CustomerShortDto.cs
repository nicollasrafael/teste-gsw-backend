﻿using Teste.Domain.Entities.ValueObjects;

namespace Teste.Domain.Resources
{
    public class CustomerShortDto
    {
        public int Id { get; set; }
        public PersonName Name { get; set; }
    }
}
