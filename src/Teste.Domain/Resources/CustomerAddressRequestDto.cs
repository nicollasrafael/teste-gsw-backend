﻿using System.ComponentModel.DataAnnotations;

namespace Teste.Domain.Resources
{
    public class CustomerAddressRequestDto
    {
        [Required] public string StreetAddress { get; set; }
        [Required] public string City { get; set; }
        [Required] public string State { get; set; }
        [Required] public string ZipCode { get; set; }
    }
}
