﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Teste.Domain.Resources
{
    public class CustomerRequestDto
    {
        [Required] public string Name { get; set; }
        [Required] public CustomerAddressRequestDto Address { get; set; }
        public List<string> Emails { get; set; }
        public List<string> Phones { get; set; }
    }
}
