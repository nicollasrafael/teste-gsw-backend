﻿using System.Collections.Generic;
using Teste.Domain.Entities.ValueObjects;

namespace Teste.Domain.Resources
{
    public class CustomerDto : CustomerShortDto
    {
        public Address Address { get; set; }
        public IEnumerable<string> Emails { get; set; }
        public IEnumerable<string> Phones { get; set; }
    }
}
