﻿using System.Linq;

namespace Teste.Domain.Entities.ValueObjects
{
    public class PersonName
    {
        public string FullName { get; protected set; }
        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }

        private PersonName() { }
        public PersonName(string fullName)
        {
            FullName = fullName;

            var splittedName = fullName.Split(' ');
            FirstName = splittedName[0];
            LastName = splittedName.Length > 1 ? string.Join(" ", splittedName.Skip(1)) : string.Empty;
        }

        public override string ToString()
        {
            return FullName;
        }
    }
}
