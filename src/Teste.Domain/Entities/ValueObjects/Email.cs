﻿namespace Teste.Domain.Entities.ValueObjects
{
    public class Email
    {
        public string Address { get; protected set; }

        private Email() { }
        public Email(string email)
        {
            Address = email;
        }

        public override string ToString()
        {
            return Address;
        }
    }
}
