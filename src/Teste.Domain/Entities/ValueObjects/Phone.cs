﻿namespace Teste.Domain.Entities.ValueObjects
{
    public class Phone
    {
        public string Number { get; private set; }

        private Phone() { }
        public Phone(string phoneNumber)
        {
            Number = phoneNumber;
        }

        public override string ToString()
        {
            return Number;
        }
    }
}
