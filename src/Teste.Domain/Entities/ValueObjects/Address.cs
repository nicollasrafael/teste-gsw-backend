﻿namespace Teste.Domain.Entities.ValueObjects
{
    public class Address
    {
        public string StreetAddress { get; protected set; }
        public string City { get; protected set; }
        public string State { get; protected set; }
        public string ZipCode { get; protected set; }

        public Address(string streetAddress, string city, string state, string zipCode)
        {
            StreetAddress = streetAddress;
            City = city;
            State = state;
            ZipCode = zipCode;
        }

        public override string ToString()
        {
            return $"{StreetAddress}, {City} - {State}, {ZipCode}";
        }
    }
}
