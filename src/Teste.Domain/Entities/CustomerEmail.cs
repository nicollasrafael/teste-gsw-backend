﻿using Teste.Domain.Entities.ValueObjects;

namespace Teste.Domain.Entities
{
    public class CustomerEmail : Entity<int>
    {
        public Customer Customer { get; protected set; }
        public Email Email { get; protected set; }

        private CustomerEmail() { }
        public CustomerEmail(Customer customer, Email email)
        {
            Customer = customer;
            Email = email;
        }

        public override string ToString()
        {
            return Email.ToString();
        }
    }
}
