﻿using Teste.Domain.Entities.ValueObjects;

namespace Teste.Domain.Entities
{
    public class CustomerPhone : Entity<int>
    {
        public Customer Customer { get; private set; }
        public Phone Phone { get; protected set; }

        private CustomerPhone() { }
        public CustomerPhone(Customer customer, Phone phone)
        {
            Customer = customer;
            Phone = phone;
        }

        public override string ToString()
        {
            return Phone.ToString();
        }
    }
}
