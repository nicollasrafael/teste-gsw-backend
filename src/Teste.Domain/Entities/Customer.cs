﻿using System.Linq;
using System.Collections.Generic;
using Teste.Domain.Entities.ValueObjects;

namespace Teste.Domain.Entities
{
    public class Customer : Entity<int>
    {
        public PersonName Name { get; protected set; }
        public Address Address { get; protected set; }
        public IReadOnlyCollection<CustomerPhone> Phones => _phones;
        public IReadOnlyCollection<CustomerEmail> Emails => _emails;

        private readonly List<CustomerPhone> _phones;
        private readonly List<CustomerEmail> _emails;

        private Customer() {
            _phones = new List<CustomerPhone>();
            _emails = new List<CustomerEmail>();
        }

        public Customer(PersonName name, Address address)
        {
            Name = name;
            Address = address;

            _phones = new List<CustomerPhone>();
            _emails = new List<CustomerEmail>();
        }

        public void UpdatePersonName(PersonName personName)
        {
            Name = personName;
        }

        public void UpdateAddress(Address personAddress)
        {
            Address = personAddress;
        }

        public void UpdateEmails(IEnumerable<Email> emails)
        {
            var updateEmailsAddressList = emails.Select(x => x.Address).ToList();
            _emails.RemoveAll(e => !updateEmailsAddressList.Contains(e.Email.Address));

            var currentEmailsAddressList = _emails.Select(e => e.Email.Address).ToList();
            var emailsToAdd = emails.Where(x => !currentEmailsAddressList.Contains(x.Address));

            foreach (var email in emailsToAdd)
            {
                _emails.Add(new CustomerEmail(this, email));
            }
        }

        public void UpdatePhones(IEnumerable<Phone> phones)
        {
            var updatePhonesNumberList = phones.Select(x => x.Number).ToList();
            _phones.RemoveAll(e => !updatePhonesNumberList.Contains(e.Phone.Number));

            var currentPhonesNumberList = _phones.Select(e => e.Phone.Number).ToList();
            var phonesToAdd = phones.Where(x => !currentPhonesNumberList.Contains(x.Number));

            foreach (var phone in phonesToAdd)
            {
                _phones.Add(new CustomerPhone(this, phone));
            }
        }
    }
}
