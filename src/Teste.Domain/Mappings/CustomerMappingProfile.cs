﻿using AutoMapper;
using Teste.Domain.Entities;
using Teste.Domain.Resources;

namespace Teste.Domain.Mappings
{
    public class CustomerMappingProfile : Profile
    {
        public CustomerMappingProfile()
        {
            CreateMap<Customer, CustomerDto>();
            CreateMap<Customer, CustomerShortDto>();
        }
    }
}
