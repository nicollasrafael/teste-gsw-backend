﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Teste.Domain.Resources;
using Teste.Domain.Services;

namespace Teste.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICustomerService _customerService;

        public CustomerController(IMapper mapper, ICustomerService customerService)
        {
            _mapper = mapper;
            _customerService = customerService;
        }

        public async Task<IEnumerable<CustomerShortDto>> Get()
        {
            var customers = await _customerService.GetAllAsync();
            return customers.Select(customer => _mapper.Map<CustomerShortDto>(customer));
        }

        [HttpPost]
        public async Task<ActionResult<CustomerDto>> Post(CustomerRequestDto customerRequestDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var customer = await _customerService.CreateAsync(customerRequestDto);

            return CreatedAtRoute("GetCustomer", new { customerId = customer.Id }, _mapper.Map<CustomerDto>(customer));
        }

        [Route("{customerId:int}", Name = "GetCustomer")]
        public async Task<ActionResult<CustomerDto>> Get(int customerId)
        {
            var customer = await _customerService.GetByIdAsync(customerId);
            if (customer == null)
                return NotFound();

            return _mapper.Map<CustomerDto>(customer);
        }

        [HttpPut]
        [Route("{customerId:int}")]
        public async Task<IActionResult> Put(int customerId, CustomerRequestDto customerRequestDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var customer = await _customerService.GetByIdAsync(customerId);
            if (customer == null)
                return NotFound();

            await _customerService.UpdateAsync(customer, customerRequestDto);

            return Ok();
        }

        [HttpDelete]
        [Route("{customerId:int}")]
        public async Task<IActionResult> Delete(int customerId)
        {
            var customer = await _customerService.GetByIdAsync(customerId);
            if (customer == null)
                return NotFound();

            await _customerService.DeleteAsync(customer);

            return Ok();
        }
    }
}
