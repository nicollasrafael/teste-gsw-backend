# TesteBackend

Teste GSW Backend

## Requirements
- [.NET Core 3.1 SDK](https://dotnet.microsoft.com/download/dotnet-core/3.1)
- [EF Core 3.x CLI .NET](https://docs.microsoft.com/pt-br/ef/core/miscellaneous/cli/dotnet)
- [Microsoft SQL Server](https://www.microsoft.com/pt-br/sql-server/sql-server-downloads)

## Applying migrations

Open the terminal at the root of the project and execute the following commands:

- `set Teste_ConnectionString=Server=.;Database=TesteDB;Trusted_Connection=True`

> Assuming you have a SQL Server instance installed with trusted connection available, otherwise make the necessary ConnectionString changes above 

- `dotnet ef database update --project=src\Teste.Infra`

## Running
- `dotnet run --project=src\Teste.API`

